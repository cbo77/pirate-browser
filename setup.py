"""
"""

from codecs import open
from os import path
import subprocess as sp
from setuptools import setup, find_packages


pr = sp.Popen('git log --oneline | wc -l', shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
pr.wait()
verno = int(pr.stdout.read().decode('utf-8')[:-1])

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

with open(path.join(here, 'src', 'pirate', 'version.py'),'w') as f:
    f.write(f"version={verno}\n")

setup(
    name='pirate-browser',

    version=f'{verno}',  # do not forget to change this

    python_requires='>=3.6',

    description='Web browser with emacs-like bindings.',

    long_description=long_description,

    url='',

    author='Christopher Boyle',

    author_email='',

    packages=find_packages('src'),
    
    package_dir={'': 'src'},

    entry_points={
        'console_scripts': [
            'pirate=pirate.main:main'
        ]
    },

    install_requires=[]
)
