from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtWebKit import *
from PyQt5.QtWebKitWidgets import *

import inspect

def printmembers(module_or_class):
    for member in inspect.getmembers(module_or_class):
        print(member)


def printsubmembers(module_or_class, submodule_or_class):
    for member in inspect.getmembers(module_or_class):
        if member[0] == submodule_or_class:
            printmembers(member[1])
