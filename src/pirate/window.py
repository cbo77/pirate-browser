import time
import os
import json

from urllib.parse import quote as urlquote
from html2text import html2text as htt

from PyQt5.QtCore import QObject, pyqtSlot, QUrl, Qt, QRegExp
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QLabel
from PyQt5.QtGui import QRegExpValidator

from pirate.bindings import binds
from pirate.buffer import PirateBuffer
from pirate.minibuffer import PirateMiniBuffer
from pirate.keyinfo import PirateKeyInfo
from pirate.infoline import PirateInfoLine
from pirate.settings import settings
from pirate.internalpages import internalpages
from pirate.htmlutils import htmlEscapeString
from pirate.decorators import verbalise, log
from pirate.version import version


class PirateWindow(QWidget):

    curKeyPressed = None
    lastKeyPressed = None
    mesg_timeout = 0
    
    def __init__(self, url):
        super().__init__()
        self.buffers = [PirateBuffer(self)]
        self.frames = [self.buffers[0]]
        self.chord = list()
        
        self.miniBuffer = PirateMiniBuffer(self)

        self.keyInfo = PirateKeyInfo() # info about keys

        self.spacer = QLabel()
        self.spacer.setText("|")

        self.info = PirateInfoLine(self)
        self.setInfoLine()
        self.initCommands()
        self.mesg_timeout = time.time()
        self.message = ''
        self.setStyleSheet(settings['stylesheet'])

        self.previousSearch = ''
        
        if url:
            self.navigate(url)
        else:
            self.navigate(settings['startupurl'])

        self.setWindowTitle()
        self.getFrames()
        self.refreshView()

    @log
    @verbalise
    def reloadPage(self):
        '''<interactive>
reloadPage

Reloads the page in the currently focused frame.'''
        return self.getFocusedBuffer().reload()

    @log
    @verbalise
    def help(self):
        '''<interactive>
help

Opens the help page.'''
        return self.loadInternal('help')
    
    @log
    def loadInternal(self, name):
        self.getFocusedBuffer().setHtml(internalpages[name])
        self.getFocusedBuffer().setStyleSheet(settings['stylesheet'].replace('191919', 'dfdfdf'))
        return True
    
    @log
    def initCommands(self):
        self.commands = dict()
        interactiveMethods = [
            (m, getattr(self, m), getattr(getattr(self, m), "__doc__")) for m in dir(self)
            if callable(getattr(self, m)) and
            not m.startswith('__') and
            getattr(getattr(self, m), "__doc__") is not None and
            'interactive' in getattr(getattr(self, m), "__doc__").split('\n')[0]
        ]
        for m, mfunc, mdoc in interactiveMethods:
            docsplit = mdoc.split('\n')
            if len(docsplit) < 3:
                argsre = ''
                usestr = ''
                description = ''
            else:
                usestr = docsplit[1]
                argsre = docsplit[2]
                description = '\n'.join(docsplit[3:])
                
            self.commands[m] = (mfunc, description, usestr, argsre)

        restr = ''
        for command in self.commands:
            restr += f'{command}{self.commands[command][3]}|'
        restr = restr[:-1]
        re = QRegExp(restr)
        self.commandValidator = QRegExpValidator(re)

        helppage = internalpages['help']
        helppage = helppage.split('\n')[:7]

        helppage.append("<h2>Functions</h2>")
        for name, (__, desc, usage, re) in self.commands.items():
            helppage.append(f"<hr /><h4>{name}</h4>Usage: <span style='font-family: Monospace;'>{htmlEscapeString(usage)}</span><br />{desc}")

        helppage.append("<hr /><h2>Bindings</h2><p>Bindings are split into 'contexts': areas where they are active. For now, just either 'buffer' (the webpage) or 'minibuffer' (the execution dialogue at the bottom of the page).</p>")
        helppage.append("<p>If the bind exist, the relevant python code is executed. The possible functions are those within the PirateWindow class (checkout the soruce for more information, I guess). This obviously a huge security concern, so be careful and all that.</p>")
        helppage.append("<p>Chords are implemented through the functions <span style='font-family: Monospace;'>requireChord</span> and <span style='font-family: Monospace;'>startChord</span>. The former takes in two required args: the list of combinations of keys that make up a chord, and the function to be called if the chord is complete. Optionally, it takes args for the function, if required. The latter function is used to start chords.</p>")
        for context, cbinds in binds.items():
            helppage.append(f"<hr /><h3>{context}</h3>")
            bs = cbinds.keys()
            acts = cbinds.values()
            helppage.append("<div><div style='display: inline-block; position: left; width: 50%;'>")
            for b in bs:
                helppage.append(f"<span style='font-family: Monospace;'>{htmlEscapeString(b)}</span><br />")
                
            helppage.append("</div><div style='display: inline-block; float: right; width: 50%;'>")
            for a in acts:
                helppage.append(f"<span style='font-family: Monospace;'>{htmlEscapeString(a)}</span><br />")
            helppage.append("</div></div><br />")
        helppage.append("<hr /></div></body></html>")
        internalpages['help'] = '\n'.join(helppage)
        return

    @log
    def getFrames(self):
        hbox = QHBoxLayout()
        for frame in self.frames:
            hbox.addWidget(frame)
        self.frameLayout = hbox

    @log
    def refreshView(self):
        vbox = QVBoxLayout()
        vbox.addLayout(self.frameLayout, 1)
        vbox.addLayout(self.info)
        vbox.setContentsMargins(0, 0, 0, 0)
        vbox.setSpacing(0)
        hbox = QHBoxLayout()
        hbox.setContentsMargins(0, 0, 0, 0)
        hbox.setSpacing(0)
        hbox.addWidget(self.keyInfo)
        hbox.addWidget(self.spacer)
        hbox.addWidget(self.miniBuffer)
        vbox.addLayout(hbox)
        self.setLayout(vbox)

    @log
    def getFocusedBuffer(self):
        try:
            return [f for f in self.frames if f.hasFocus()][0]
        except:
            return self.frames[0]

    @log
    def openNewWindow(self, url=''):
        pass
        
    @log
    @verbalise
    def navigate(self, *args):
        '''<interactive>
navigate <url>|<search-term>
.*
Navigate to a webpage, or search using the default search engine.

Piority will be to resolve input as bookmark, then will try to 
parse the argument as a url, if parsing fails the argument is taken 
to be a search term.'''
        if len(args) > 1:
            url_str = ' '.join(args)
        elif len(args):
            url_str = args[0]
            try:
                url_str = settings['bookmarks'][args[0]]
            except:
                pass
        else:
            raise TypeError("navigate needs 1 argument (str): bookmark name, url, or search term")

        url = QUrl(url_str)
        if url.host():
            self.mesg("navigate: "+url_str)
            self.getFocusedBuffer().setUrl(url)
        else:
            url_str = settings['search-engines']['default'].format(
                urlquote(url_str, safe='')
            )
            self.mesg("navigate: searching "+url_str)
            url = QUrl(url_str)
            self.getFocusedBuffer().setUrl(url)
        return True

    @log
    @verbalise
    def historyBack(self, *args):
        '''<interactive>
historyBack

Go back in buffer's history.'''
        self.getFocusedBuffer().back()
        if args: self.focusRecentFrame()
        return True

    @log
    @verbalise
    def historyForward(self, *args):
        '''<interactive>
historyForward

Go forward in buffer's history.'''
        self.getFocusedBuffer().forward()
        if args: self.focusRecentFrame()
        return True

    @log
    @verbalise
    def quit(self):
        '''<interactive>
quit

Quit pirate-browser.'''
        exit(0)

    @log
    def commandGetMatches(self, text=None):
        if not text: text = self.miniBuffer.text()
        try:
            return [command for command in self.commands if command.startswith(text.split()[0])]
        except IndexError:
            return []

    @log
    def commandTabComplete(self, text=None):
        if not text: text = self.miniBuffer.text()
        matches = self.commandGetMatches(text)
        longestCompletion = matches[0]
        for match in matches[1:]:
            while longestCompletion not in match:
                longestCompletion = longestCompletion[:-1]

        if longestCompletion in self.commands: longestCompletion += ' '
        return longestCompletion

    @log
    def miniBufferTextChanged(self, text):
        if self.miniBuffer.hasFocus():
            # user in minibuffer

            matches = self.commandGetMatches(text)
            if text in self.commands or len(matches) == 1:
                self.mesg(self.commands[matches[0]][2])
            else:
                self.mesg(' '.join(matches))
            
            if self.miniBuffer.validator() is None:
                self.miniBuffer.setValidator(self.commandValidator)
        else:
            if self.miniBuffer.validator() is not None:
                self.miniBuffer.setValidator(None)                

    @log
    def focusMiniBuffer(self, text=''):
        self.miniBuffer.setText(text)
        self.miniBuffer.setFocus()
        return True

    @log
    def focusRecentFrame(self):
        self.frames[0].setFocus()
        return True

    @log
    @verbalise
    def startChord(self, name):
        self.chord.append(name)

    @log
    def resetChord(self):
        self.chord = list()

    @log
    def exitMiniBuffer(self):
        self.miniBuffer.reset()
        self.focusRecentFrame()

    @log
    @verbalise
    def requireChord(self, chord, callback, *args):
        if self.chord == chord:
            callback(*args)
            return True
        return False

    @log
    def checkBinding(self, context):
        print(self.curKeyPressed)
        try:
            res = binds[context][self.curKeyPressed.name]
            print(res)
            return eval(res)
        except KeyError:
            self.resetChord()
            return False

    @log
    def setPressedKeys(self, key):
        self.curKeyPressed = key
        if key != None and not self.miniBuffer.hasFocus():
            self.lastKeyPressed = key
        self.keyInfo.setText(str(self.lastKeyPressed))

    @log
    def setInfoLine(self, message=''):
        buf = self.getFocusedBuffer()
        frame = buf.page().mainFrame()
        title = buf.getUrl()
        mxpos = frame.scrollBarMaximum(0)
        mnpos = frame.scrollBarMinimum(0)
        pos = frame.scrollBarValue(0)
        if pos == mnpos:
            pos = 'top'
        elif pos == mxpos:
            pos = 'bot'
        else:
            pos = f'{float(pos*100)/(float(mxpos)-float(mnpos)):.0f}%'
        if not message and time.time() < self.mesg_timeout: message = self.message
        self.info.setInfo(self.getFocusedBuffer().status, title, message, pos)

    @log
    def setWindowTitle(self):
        title = self.getFocusedBuffer().getUrl()
        title += f" : pirate browser"
        super().setWindowTitle(title)

    @log
    def mesg(self, text):
        self.setInfoLine(text)
        self.mesg_timeout = time.time() + 10
        self.message = text

    @log
    def wheelEvent(self, *args, **kwargs):
        super().wheelEvent(*args, **kwargs)
        self.setInfoLine()

    @log
    @verbalise
    def executeCommand(self):
        mbufText = str(self.miniBuffer.text()).split()
        command = mbufText[0]
        print(f"executing {command}")
        args = list()
        for arg in mbufText[1:]: args.append(arg)
        args = tuple(args)
        
        try:
            mfunc, __, __, __ = self.commands[command]
            mfunc(*args)
        except KeyError:
            print("  keyerror")
            match = self.commandTabComplete()
            completions = ''
            for match in self.commandGetMatches():
                completions += match + " "
            self.mesg(f"Command '{command}' not found. Possible matches: {completions}")
            self.miniBuffer.setText(match)
        except TypeError as ex:
            print("  typeerror")
            self.mesg(f"{str(ex)}")
            self.miniBuffer.reset()
        else:
            self.miniBuffer.reset()
        return

    @log
    def getFocusedUrl(self):
        return self.getFocusedBuffer().getUrl()
    
    @log
    @verbalise
    def addBookmark(self, name, url):
        '''<interactive>
addBookmark <name> <url>
[\w-_]* .*
Add a bookmark. 

<name> is an easy to type alias for accessing the bookmark, <url> is url.

i.e. once you've added a bookmark you can navigate to its url by doing:

C-x C-f <name> <enter>
'''
        if name in settings['bookmarks']:
            self.mesg("Updating existing bookmark")
        else:
            self.mesg("Creating new bookmark")
            
        settings['bookmarks'][name] = url

        self.saveSettings()

    @log
    @verbalise
    def hint(self):
        '''<interactive>'''
        self.getFocusedBuffer().hint()

    @log
    @verbalise
    def incrementalSearch(self, term=''):
        '''<interactive>
search [<term>]
.*
Searches current page for text.'''
        if not term: term = self.previousSearch
        if not term: term = ' '.join(self.miniBuffer.text().split()[1:])
        if not term: raise TypeError("incrementalSearch was passed no search term and no previous search exists!")
        self.getFocusedBuffer().search(term)
        self.previousSearch = term

    @log
    @verbalise
    def cancel(self):
        self.exitMiniBuffer()

    @log
    def saveSettings(self):
        '''<interactive>
saveSettings

Saves settings to ~/.config/pirate-browser/config.json, 
creating the file if not exists (and directories in the path)'''

        try:
            os.makedirs(os.path.expanduser("~/.config/pirate-browser"))
        except:
            pass

        with open(os.path.expanduser("~/.config/pirate-browser/config.json"),"w") as configf:
            json.dump(settings, configf)
