import json
import os

from PyQt5.QtGui import QFont


default_settings = {
    "bookmarks" : {
        "home" : "https://google.com"
    },
    "search-engines" : {
        "default" : "https://www.google.com/search?q={}"
    },
    "startupurl" : "https://www.google.com",
    "stylesheet" : '''
color: #DFDFDF;
background: #191919;
font-family: Monospace;
font-size: 16;
'''
}

try:
    with open(os.path.expanduser("~/.config/pirate-browser/config.json")) as configf:
        settings = {
            **default_settings,
            **(json.load(configf))
        }
except:
    settings = default_settings
