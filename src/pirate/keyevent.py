from PyQt5.QtCore import Qt

qKeyNames = dict()
qKeyNames[Qt.Key_A] = 'a'
qKeyNames[Qt.Key_B] = 'b'
qKeyNames[Qt.Key_C] = 'c'
qKeyNames[Qt.Key_D] = 'd'
qKeyNames[Qt.Key_E] = 'e'
qKeyNames[Qt.Key_F] = 'f'
qKeyNames[Qt.Key_G] = 'g'
qKeyNames[Qt.Key_H] = 'h'
qKeyNames[Qt.Key_I] = 'i'
qKeyNames[Qt.Key_J] = 'j'
qKeyNames[Qt.Key_K] = 'k'
qKeyNames[Qt.Key_L] = 'l'
qKeyNames[Qt.Key_M] = 'm'
qKeyNames[Qt.Key_N] = 'n'
qKeyNames[Qt.Key_O] = 'o'
qKeyNames[Qt.Key_P] = 'p'
qKeyNames[Qt.Key_Q] = 'q'
qKeyNames[Qt.Key_R] = 'r'
qKeyNames[Qt.Key_S] = 's'
qKeyNames[Qt.Key_T] = 't'
qKeyNames[Qt.Key_U] = 'u'
qKeyNames[Qt.Key_V] = 'v'
qKeyNames[Qt.Key_W] = 'w'
qKeyNames[Qt.Key_X] = 'x'
qKeyNames[Qt.Key_Y] = 'y'
qKeyNames[Qt.Key_Z] = 'z'

qKeyNames[Qt.Key_0] = '0'
qKeyNames[Qt.Key_1] = '1'
qKeyNames[Qt.Key_2] = '2'
qKeyNames[Qt.Key_3] = '3'
qKeyNames[Qt.Key_4] = '4'
qKeyNames[Qt.Key_5] = '5'
qKeyNames[Qt.Key_6] = '6'
qKeyNames[Qt.Key_7] = '7'
qKeyNames[Qt.Key_8] = '8'
qKeyNames[Qt.Key_9] = '9'

qKeyNames[Qt.Key_Ampersand] = '&'
qKeyNames[Qt.Key_Apostrophe] = '\''
qKeyNames[Qt.Key_Asterisk] = '*'
qKeyNames[Qt.Key_At] = '@'
qKeyNames[Qt.Key_Backslash] = '\\'
qKeyNames[Qt.Key_BraceLeft] = '{'
qKeyNames[Qt.Key_BraceRight] = '}'
qKeyNames[Qt.Key_BracketLeft] = '['
qKeyNames[Qt.Key_BracketRight] = ']'
qKeyNames[Qt.Key_Colon] = ':'
qKeyNames[Qt.Key_Comma] = ','
qKeyNames[Qt.Key_Dollar] = '$'
qKeyNames[Qt.Key_Equal] = '='
qKeyNames[Qt.Key_Greater] = '>'
qKeyNames[Qt.Key_Less] = '<'
qKeyNames[Qt.Key_Minus] = '-'
qKeyNames[Qt.Key_NumberSign] ='#'
qKeyNames[Qt.Key_Percent] = '%'
qKeyNames[Qt.Key_Period] ='.'
qKeyNames[Qt.Key_Plus] ='+'
qKeyNames[Qt.Key_ParenLeft] = '('
qKeyNames[Qt.Key_ParenRight] = ')'
qKeyNames[Qt.Key_QuoteDbl] = '"'
qKeyNames[Qt.Key_QuoteLeft] = '`'
qKeyNames[Qt.Key_Semicolon] = ';'
qKeyNames[Qt.Key_Slash] = '/'
qKeyNames[Qt.Key_Space] = '<space>'
qKeyNames[Qt.Key_Underscore] = '_'
qKeyNames[Qt.Key_Exclam] = '!'
qKeyNames[Qt.Key_hyphen] = '--'
qKeyNames[Qt.Key_sterling] = '£'


qKeyNames[Qt.Key_Back] = '<back>'
qKeyNames[Qt.Key_Forward] = '<forward>'
qKeyNames[Qt.Key_CapsLock] = '<capslock>'
qKeyNames[Qt.Key_Insert] = '<insert>'
qKeyNames[Qt.Key_Delete] = '<del>'
qKeyNames[Qt.Key_Backspace] = '<backspace>'
qKeyNames[Qt.Key_Enter] = '<enter>'
qKeyNames[Qt.Key_Return] = '<enter>'
qKeyNames[Qt.Key_Escape] = '<escape>'
qKeyNames[Qt.Key_PageDown] = '<pgdn>'
qKeyNames[Qt.Key_PageUp] = '<pgup>'
qKeyNames[Qt.Key_Direction_L] = '<key_dir_l>'
qKeyNames[Qt.Key_Direction_R] = '<key_dir_r>'
qKeyNames[Qt.Key_Tab] = '<tab>'
qKeyNames[Qt.Key_Backtab] = '<backtab>'
qKeyNames[Qt.Key_Home] = '<home>'
qKeyNames[Qt.Key_End] = '<end>'
qKeyNames[Qt.Key_Menu] = '<menu>'
qKeyNames[Qt.Key_Meta] = '<meta>'
qKeyNames[Qt.Key_Left] = '<left>'
qKeyNames[Qt.Key_Right] = '<right>'
qKeyNames[Qt.Key_Up] = '<up>'
qKeyNames[Qt.Key_Down] = '<down>'

qKeyNames[Qt.Key_F1] = 'f1'
qKeyNames[Qt.Key_F10] = 'f10'
qKeyNames[Qt.Key_F11] = 'f11'
qKeyNames[Qt.Key_F12] = 'f12'
qKeyNames[Qt.Key_F13] = 'f13'
qKeyNames[Qt.Key_F14] = 'f14'
qKeyNames[Qt.Key_F15] = 'f15'
qKeyNames[Qt.Key_F16] = 'f16'
qKeyNames[Qt.Key_F17] = 'f17'
qKeyNames[Qt.Key_F18] = 'f18'
qKeyNames[Qt.Key_F19] = 'f19'
qKeyNames[Qt.Key_F2] = 'f2'
qKeyNames[Qt.Key_F20] = 'f20'
qKeyNames[Qt.Key_F21] = 'f21'
qKeyNames[Qt.Key_F22] = 'f22'
qKeyNames[Qt.Key_F23] = 'f23'
qKeyNames[Qt.Key_F24] = 'f24'
qKeyNames[Qt.Key_F25] = 'f25'
qKeyNames[Qt.Key_F26] = 'f26'
qKeyNames[Qt.Key_F27] = 'f27'
qKeyNames[Qt.Key_F28] = 'f28'
qKeyNames[Qt.Key_F29] = 'f29'
qKeyNames[Qt.Key_F3] = 'f3'
qKeyNames[Qt.Key_F30] = 'f30'
qKeyNames[Qt.Key_F31] = 'f31'
qKeyNames[Qt.Key_F32] = 'f32'
qKeyNames[Qt.Key_F33] = 'f33'
qKeyNames[Qt.Key_F34] = 'f34'
qKeyNames[Qt.Key_F35] = 'f35'
qKeyNames[Qt.Key_F4] = 'f4'
qKeyNames[Qt.Key_F5] = 'f5'
qKeyNames[Qt.Key_F6] = 'f6'
qKeyNames[Qt.Key_F7] = 'f7'
qKeyNames[Qt.Key_F8] = 'f8'
qKeyNames[Qt.Key_F9] = 'f9'

qMs = [('s-', Qt.ShiftModifier), ('C-', Qt.ControlModifier), ('M-', Qt.AltModifier)]
qMods = [
    [                 qMs[0] ],
    [         qMs[1]         ],
    [         qMs[1], qMs[0] ],
    [ qMs[2]                 ],
    [ qMs[2],         qMs[0] ],
    [ qMs[2], qMs[1]         ],
    [ qMs[2], qMs[1], qMs[0] ],
]

for combination in qMods:
    kcode = 0
    kname = ''
    for name, code in combination:
        if kcode == 0:
            kcode = code
        else:
            kcode = kcode | code
        kname += name
    qKeyNames[kcode] = name

class SensibleKeyEvent(object):

    def __init__(self, qKeyEvent):
        try:
            self.name = qKeyNames[int(qKeyEvent.modifiers())]
        except KeyError:
            self.name = ''

        try:
            self.name += qKeyNames[qKeyEvent.key()]
        except KeyError:
            self.name = '? '+str(qKeyEvent.key())


    def __repr__(self):
        return f"Key: {self.name}"

    def __str__(self):
        return self.name
