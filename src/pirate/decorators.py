
def verbalise(f):
    def nf(*args, **kwargs):
        args[0].mesg(f.__name__)
        return f(*args, **kwargs)
    nf.__doc__ = f.__doc__
    return nf

def log(f):
    def nf(*args, **kwargs):
        print("Log:", f.__name__, *args, **kwargs)
        return f(*args, **kwargs)
    nf.__doc__ = f.__doc__
    return nf
