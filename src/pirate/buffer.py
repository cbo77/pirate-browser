from html2text import html2text as htt

from PyQt5.QtWebKitWidgets import QWebView
from PyQt5.QtCore import QEvent, Qt

from pirate.keyevent import SensibleKeyEvent

class PirateBuffer(QWebView):

    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.loadStarted.connect(self.bufferLoading)
        self.loadFinished.connect(self.bufferLoaded)
        self.hinted = -1
        self.status = "-:%%-"

    def keyPressEvent(self, event):
        skey = SensibleKeyEvent(event)
        self.parent.setPressedKeys(skey)
        if not self.parent.checkBinding('buffer'): super().keyPressEvent(event)
        
    def keyReleaseEvent(self, event):
        self.parent.setPressedKeys(None)
        super().keyReleaseEvent(event)
        
    def event(self, event):
        if (event.type()==QEvent.KeyPress) and (event.key()==Qt.Key_Tab):
            self.keyPressEvent(event)
            return True
        return super().event(event)

    def bufferLoading(self, *args, **kwargs):
        print(f"buffer loading url \'{self.getUrl()}\'", args, kwargs)
        self.status = "-:%*-"
        self.parent.setInfoLine()

    def bufferLoaded(self, *args, **kwargs):
        print("buffer finished loading", args, kwargs)
        self.status = "-:%%-"
        self.parent.setInfoLine()
        #self.hint()

    def hint(self):
        self.elements = list(self.page().mainFrame().findAllElements("*"))
        print(self.elements)
        self.clickables = [(htt(e.toInnerXml()), e.geometry(), e) for e in self.elements if not htt(e.toInnerXml()).startswith("!")]
        self.clickables = [(ihtml.strip(), geom, e) for ihtml, geom, e in self.clickables if ihtml.strip()]

        for c in self.clickables:
            __, __, e = c
            if e.hasAttribute('target'):
                print(e)
                e.removeAttribute('target')
            #print(c)

        if self.hinted > len(self.clickables): self.hinted = -1

        self.hinted += 1
        print("focusing", self.clickables[self.hinted])
        self.clickables[self.hinted][2].setFocus()

    def search(self, search_pattern):
        self.findText(search_pattern)

        

    def getUrl(self):
        return self.url().toDisplayString()
