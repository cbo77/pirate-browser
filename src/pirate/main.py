import sys

from docopt import docopt
from PyQt5.QtWidgets import QApplication

from pirate.window import PirateWindow

def main(): 
    usage = 'Usage: pirate [<url>]'
    args = docopt(usage, version="1")
    app = QApplication(sys.argv)
    view = PirateWindow(args['<url>'])
    view.show()
    app.exec()
