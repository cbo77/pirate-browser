from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtCore import Qt, QEvent

from pirate.keyevent import SensibleKeyEvent

class PirateMiniBuffer(QLineEdit):

    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.setFrame(False)
        self.textChanged.connect(self.parent.miniBufferTextChanged)
        #self.setFont(font)
        
    def keyPressEvent(self, event):
        skey = SensibleKeyEvent(event)
        self.parent.setPressedKeys(skey)
        if not self.parent.checkBinding('minibuffer'): super().keyPressEvent(event)
        
    def keyReleaseEvent(self, event):
        self.parent.setPressedKeys(None)
        super().keyReleaseEvent(event)

    def event(self, event):
        if (event.type()==QEvent.KeyPress) and (event.key()==Qt.Key_Tab):
            self.keyPressEvent(event)
            return True
        return super().event(event)

    def reset(self):
        self.setText('')
        self.parent.focusRecentFrame()
