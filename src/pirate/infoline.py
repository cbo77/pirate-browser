from PyQt5.QtWidgets import QHBoxLayout, QLabel, QPushButton

from pirate.version import version

class PirateInfoLine(QHBoxLayout):
    def __init__(self, parent):
        super().__init__()

        self.parent = parent
        spacer = QLabel()
        spacer.setText(" | ")
        
        self.addSpacing(10)
        self.status = QLabel()
        self.addWidget(self.status)

        self.addSpacing(10)
        self.bufferTitle = QLabel()
        self.addWidget(self.bufferTitle)
        
        self.addStretch()
        
        self.position = QLabel()
        self.addWidget(self.position)

        self.addSpacing(5)
        
        self.backButton = QPushButton(" < ")
        self.backButton.clicked.connect(self.parent.historyBack)
        self.backButton.setStyleSheet("border-style: none;")
        self.addWidget(self.backButton)

        self.addSpacing(5)
        
        self.forwardButton = QPushButton(" > ")
        self.forwardButton.clicked.connect(self.parent.historyForward)
        self.forwardButton.setStyleSheet("border-style: none;")
        self.addWidget(self.forwardButton)

        
        self.addWidget(spacer)
        
        self.version = QLabel()
        self.version.setText(f'v{version}')
        self.version.setFixedWidth(35)
        self.addWidget(self.version)
        
        self.addSpacing(10)

    def setInfo(self, status, bufferTitle, message, position):
        self.status.setText(status)
        self.position.setText(position)
        if message: self.bufferTitle.setText(message)
        else: self.bufferTitle.setText(bufferTitle)
