binds = {
    'buffer': {
        'C-x' : "self.startChord('C-x')",
        'C-c' : "self.requireChord(['C-x'], self.quit)",
        'k' : "self.requireChord(['C-x'], self.focusMiniBuffer, 'quit ')",
        'C-f' : "self.requireChord(['C-x'], self.focusMiniBuffer, 'navigate ', )",
        'C-w' : "self.requireChord(['C-x'], self.focusMiniBuffer, f'navigate {self.getFocusedUrl()}', )",
        'C-g' : "self.requireChord(['C-x'], self.navigate, 'https://google.com')",
        'M-x' : "self.focusMiniBuffer()",
        'C-/' : "self.historyBack()",
        'C-s' : "self.focusMiniBuffer(f'incrementalSearch {self.previousSearch}')",
        'f5' : "self.reloadPage()"
    },
    'minibuffer' : {
        'C-g' : "self.cancel()",
        '<escape>' : "self.cancel()",
        '<enter>' : "self.executeCommand()",
        '<tab>' : "self.focusMiniBuffer(self.commandTabComplete())",
        'C-s' : "self.incrementalSearch()"
    }
}
