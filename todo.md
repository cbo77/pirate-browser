TODO
====
Stuff to add

* Bookmarks management (~adding~, removing, ~editing~)
* In-page (reversible) search
* Local settings to override custom settings
* Buffers (websites open, not necessarily in the foreground)
* Tabs (views into buffers)
* Windows (instances of pirate)
* Download management (i.e. being able to handle it at all)
* Extensions
* Psuedo shell (could be used for download/bookmark/setting management)
* History
