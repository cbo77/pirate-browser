pirate-browser
==============

Web browser, written in python, with emacs-like bindings.


Key Bindings
------------

Notation is same as in emacs. Prefixed letters and dashes show
modifiers: `C-` is control, `M-` is alt, `s-` is shift. Spaces
separate items in a chord (set of binds executed in order to
perform an action).

Only a few things have been implemented at the moment:

 * `C-x C-c` quit
 * `C-x C-f` navigate
 * `C-/` back
 * `M-x` execute command in minibuffer


Dependencies
------------

Packages required:
 * python (>3.6) because f-strings ([PEP 498](https://www.python.org/dev/peps/peps-0498/)) are the absolute bomb

Python modules (should be resolved during installation, here's a list anyway):
 * docopt because args are hard
 * html2text because hinting is hard
 * pyqt5 for the sexy gui and webkit browser implementation


Installation
------------

Clone the repository, then:

```bash
sudo python setup.py install
```


Work in Progress
----------------
Lemme know if anything goes wrong! Steps leading to the
crash/bug/error would be greatly appreciated.


License
-------

MIT. See LICENSE file included with this repository for more information.